NPCharacter = new Mongo.Collection('n_p_character');

NPCharacter.attachSchema(new SimpleSchema({

  objectCode:{
    type:String
  },
  name:{
    type:String
  },
  npCharacterDescription:{
    type:String
  },
  image:{
    type:String
  },
  asksFor:{
    type:String
  },
  npCharacterRequestCodes:{
    type:[String]
  },
  scenarioCode:{
    type:[String]
  },
  playerResponseCodes:{
    type:[String]
  }
}));

//DONE 4/21/15