NPCharacterRequest = new Mongo.Collection('n_p_character_request');

NPCharacterRequest.attachSchema(new SimpleSchema({

  objectCode:{
    type:String
  },
  title:{
    type:String
  },
  npCharacterResponseCode:{
    type:[String]
    },
/*  itemId:{
    type:String
  },
  speech:{
    type:String
  },
 */
  playerResponseCodes:{
    type:[String]
  }
}));

//DONE 4/21/15