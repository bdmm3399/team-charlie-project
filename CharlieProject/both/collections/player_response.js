PlayerResponse = new Mongo.Collection('player_response');

PlayerResponse.attachSchema(new SimpleSchema({

    objectCode:{
        type:String
    },
    title:{
      type: [String]
    },
    /*playerChoice:{
        type: Number,
        optional:true

    },*/
    npCharacterCode:{
        type: [String]
    },
    isCorrectAnswer:{
        type:Number
    },
    humanGood: {
        type:Boolean,
        optional:true
    },
    goblinGood: {
        type:Boolean,
        optional:true
    },
    dwarfGood: {
        type:Boolean,
        optional:true
    }
}));

/*
 * Add query methods like this:
 *  PlayerResponse.findPublic = function () {
 *    return PlayerResponse.find({is_public: true});
 *  }
 */

//DONE 4/21/15