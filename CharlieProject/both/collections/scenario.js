Scenario = new Mongo.Collection('scenario');

Scenario.attachSchema(new SimpleSchema({

  objectCode:{
    type:String
  },
  title:{
    type:String
  },
  characterCode:{
    type:String,
    optional:true
  },
  itemCode:{
    type:String,
    optional:true
  }/*
  settingNumber: {
    type: Number,
    optional:true
  }*/
}));

/*
 * Add query methods like this:
 *  Scenario.findPublic = function () {
 *    return Scenario.find({is_public: true});
 *  }
 */

//DONE 4/21/15