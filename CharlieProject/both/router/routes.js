/*****************************************************************************/
/* Client and Server Routes */
/*****************************************************************************/
Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});

Router.route('/', {name: 'home'});
Router.route('/scenarios', {name: 'scenarios'}); /*done*/
Router.route('/npcharacters', {name: 'npcharacter'}); /*done*/
Router.route('/npcharacterrequest', {name: 'npcharacterrequest'}); /*done*/
Router.route('/npcharacterresponse', {name: 'npcharacterresponse'}); /*done*/
Router.route('/playerresponse', {name: 'playerresponse'}); /*done*/
Router.route('/playerrequest', {name: 'playerrequest'}); /*done*/
Router.route('/items', {name: 'items'});
Router.route('/play', {name: 'play'});
