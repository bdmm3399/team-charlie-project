/**
 * Created by pstensg on 4/22/2015.
 */
Template.items.events({
    /*
     * Example:
     *  'click .selector': function (e, tmpl) {
     *
     *  }
     */
});

Template.items.helpers({
    items: function () {
        return Item.find({});
    }
});

/*****************************************************************************/
/* Scenarios: Lifecycle Hooks */
/*****************************************************************************/
Template.items.created = function () {
};

Template.items.rendered = function () {
};

Template.items.destroyed = function () {
};