/*****************************************************************************/
/* ProcessPlay Methods */
/*****************************************************************************/

Meteor.methods({
    '/app/processPlay': function (scenario, npCharacter, /*npCharacterRequest, playerResponseCode,*/ player_response) {

        var user = Meteor.user();

        var score = user.profile.score;

        var choice = user.profile.choice;

        //var base = user.profile.base;

        if (score === undefined) {

            score = 0;
        }
        if (choice === undefined) {
            choice = 0;
        }

       /* var firstTime;
        firstTime = true;

        if (firstTime === true) {
            base = 0;
            firstTime = false;
        }

        var hitMessage;
*/
        // Evaluate input elements
        // In this case, results are completely random
       // var randomResult = Math.random() < 0.5 ? true : false; // http://stackoverflow.com/questions/9730966/how-to-decide-between-two-numbers-randomly-using-javascript



        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }
        var imageNumber = getRandomInt(0, 3);
        var imageName = "name";
        console.log(imageNumber);
        switch (imageNumber) {
            case 0:
                imageName = "/human.jpg";
                break;
            case 1:
                imageName = "/goblin.jpg";
                break;
            case 2:
                imageName = "/dwarf.jpg";
                break;
        }
        var resultMessage;
        var generatedScenario = Scenario.findOne({}).objectCode;

        if(choice === null) {
            console.log("choice doesn't exist");
        }
        if(choice != null) {
            console.log( "Choice is: "+ choice + "and imageNumber is: " + imageNumber);
        switch (imageNumber) {

            case 0: if(choice == imageNumber) {
                resultMessage = "Thanks!";
                score++;
            } else {
                resultMessage = "I didn't want that!";
                score--;
            }
                break;
            case 1: if (choice == imageNumber) {
                    resultMessage = "Thanks!";
                    score++;
            } else {
                    resultMessage = "I didn't want that!";
                    score--;
                }
                break;
            case 2: if (choice == imageNumber) {
                    resultMessage = "Thanks!";
                    score++;
                } else {
                resultMessage = "I didn't want that!";
                score--;
                }
                break;
        }
    }
        //var setResult = true;
/*
        if (randomResult === true) {
            hitMessage = "You hit it!";
            base++;
            // If necessary increment user's score
        } else {
            hitMessage = "You miss!";
            base = 0;
        }
        if (base === 4) {
            score++;
            base = 0;
        }

        // If necessary select a
        // new scenario,
      */var scenarios = Scenario.find({}).fetch();
        var newScenario = scenarios[imageNumber];
// character,
        var npCharacters = NPCharacter.find({}).fetch ();
        var newNPCharacter = npCharacters[imageNumber];

        //new player request
        var playerResponse = PlayerResponse.find({}).fetch ();
        var newPlayerResponse = playerResponse[imageNumber];
        // and request,
        //var requests = NPCharacterRequest.find ({objectCode: {$in: randomCharacter.npCharacterRequestCodes}}).fetch ();

        // and return them to the template


        Meteor.users.update({_id: Meteor.user()._id}, {$set: {"profile.score": score}});
        Meteor.users.update({_id: Meteor.user()._id}, {$set: {"profile.choice": choice}});

        var response = {};
        response.randomScenario = true;
        response.playResult = resultMessage;
        response.newScenario = newScenario;
        response.newNPCharacter = newNPCharacter;
        //response.newNPCharacterRequest = true;
        response.newPlayerResponse = newPlayerResponse;
      //  response.newPlayerResponseCode = playerResponse;
        //response.wasHit = randomResult;
        response.characterImageNumber = imageNumber;
        response.characterImage = imageName;

        response.helloWord = function () {
            console.log('Hello World');
        };

        response.helloWord();


        return response;
    }
});